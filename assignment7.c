#include<stdio.h>
int main()
{
int a[100][100],b[100][100],multiply[100][100];
int m,n,p,q,i,j,k;
printf("\n Enter the number of rows in the first matrix");
scanf("%d",&m);
printf("\n Enter the number of columns in the first matrix");
scanf("%d",&n);
printf("\n Enter the number of rows in the second matrix");
scanf("%d",&p);
printf("\n Enter the number of columns in the second matrix");
scanf("%d",&q);
if(n==p)
{
printf("\n Enter the elements of the first matrix:");
for(i=0;i<m;i++)
{
for(j=0;j<n;j++)
{
scanf("%d",&a[i][j]);
}
}
printf("\n Enter the elements of the second matrix");
for(i=0;i<p;i++)
{
for(j=0;j<q;j++)
{
scanf("%d",&b[i][j]);
}
}
printf("\n The first matrix");
for(i=0;i<m;i++)
{
for(j=0;j<n;j++)
{
printf("%d\t",a[i][j]);
}
printf("\n");
}
printf("\n The second matrix");
for(i=0;i<p;i++)
{
for(j=0;j<q;j++)
{
printf("%d\t",b[i][j]);
}
printf("\n");
}
for(i=0;i<m;i++)
{
for(j=0;j<q;j++)
{
multiply[i][j]=0;
for(k=0;k<p;k++)
{
multiply[i][j]=multiply[i][j]+a[i][k]*b[k][j];
}
}
}
printf("\n The resultant matrix after multiplication is :");
for(i=0;i<m;i++)
{
for(j=0;j<q;j++)
{
printf("%d\t",multiply[i][j]);
}
printf("\n");
}
}
else
{
printf("\n The multiplication is not possible");
}
return 0;
}
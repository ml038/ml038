#include <stdio.h>
void swap(int *u, int *v);
int main()
{
int x,y;
printf("Enter two numbers: ");
scanf("%d%d",&x,&y);
printf("Before swapping in main n");
printf("Value of x = %d \n", x);
printf("Value of y = %d \n\n", y);
swap(&x,&y);
printf("After swapping in main n");
printf("Value of x = %d \n", x);
printf("Value of y = %d \n\n", y);
return 0;
}
void swap(int *a,int *b)
{
int temp;
temp = *a;
*a = *b;
*b= temp;
}
